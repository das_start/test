<?php
/**
 * Plugin Name: World Countries
 * Description: Loaded data from api and display by shortcode in table.
 * Version: 1.0
 * Author: Alex Doroshenko
 */

use Classes\WorldCountries;

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

if ( ! defined( 'WC_PLUGIN_FILE' ) ) {
    define( 'WC_PLUGIN_FILE', __FILE__ );
}

if ( ! defined( 'WC_PLUGIN_DIR' ) ) {
    define( 'WC_PLUGIN_DIR', plugin_dir_path(WC_PLUGIN_FILE) );
}

require_once 'routing.php';
require_once 'vendor/autoload.php';

(new WorldCountries())->run();