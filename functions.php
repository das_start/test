<?php

function countries(WP_REST_Request $request)
{
    $countries = \Classes\WorldCountries::countries();
    return wp_send_json($countries, 200);
}