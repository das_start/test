<?php
/**
 * Template for rendering tables
 */
?>

<table id="dataTable" class="display" style="width:100%">
    <thead>
    <tr>
        <th>Name</th>
        <th>Capital</th>
        <th>Region</th>
        <th>Population</th>
        <th>Timezones</th>
        <th>Languages</th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th>Name</th>
        <th>Capital</th>
        <th>Region</th>
        <th>Population</th>
        <th>Timezones</th>
        <th>Languages</th>
    </tr>
    </tfoot>
</table>
