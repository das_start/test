<?php
/**
 * Routing file
 * */

require_once "functions.php";

add_action( 'rest_api_init', 'plugin_endpoints');
function plugin_endpoints()
{
    register_rest_route( 'world-countries/v1', 'countries', array(
        'methods'  => 'GET',
        'callback' => 'countries'
    ));
}