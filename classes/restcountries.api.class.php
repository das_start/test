<?php

namespace Classes;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class RestCountriesApi
{
    /**
     *
     */
    const API_LINK = 'https://restcountries.eu/rest/v2/';
    /**
     *
     */
    const API_SERVICE = 'all';
    /**
     *
     */
    const REQUIRED_FIELDS = ['name', 'capital', 'region', 'population', 'timezones', 'languages'];
    /**
     * @var
     */
    protected $countries;

    /**
     * RestCountriesApi constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCountries()
    {
        $requestParams = implode(';', self::REQUIRED_FIELDS);

        $client = new Client([
            'base_uri' => self::API_LINK,
        ]);

        try {
            $countries = json_decode($client->request('GET', 'all', ['query'=>"fields={$requestParams}"])->getBody()->getContents());
            return $countries;

        } catch (RequestException $e) {
            echo $e->getRequest() . "\n";
            if ($e->hasResponse()) {
                echo $e->getResponse() . "\n";
            }
            die('error!');
        }
    }

    /**
     * @return bool
     */
    public function countriesExist()
    {
        global $wpdb;
        $table = $wpdb->prefix . WorldCountries::DB_TABLE_NAME;

        return !empty($wpdb->get_results( "SELECT id FROM {$table} LIMIT 1" ));
    }

    /**
     * @param array $countriesInfo
     * @return bool
     */
    public function saveCountry($countryInfo = [])
    {
        global $wpdb;

        if (empty($countryInfo)) {
            return false;
        }

        try {
            $table_name = $wpdb->prefix . WorldCountries::DB_TABLE_NAME;

            $wpdb->insert(
                $table_name,
                (array)$countryInfo
            );
        } catch (\Exception $e) {
            return false;
        }
    }

}