<?php

namespace Classes;


class WorldCountries
{
    /**
     *
     */
    const DB_TABLE_NAME = 'world_countries';

    /**
     * WorldCountries constructor.
     */
    public function __construct()
    {
    }

    /**
     *
     */
    public function run()
    {
        //init
        $this->init();
    }

    /**
     *
     */
    public function init()
    {
        $this->afterActivation();

        //default styles and scripts
        add_action('wp_enqueue_scripts', [$this, 'enqueue_scripts_styles']);

        //register shortcode
        add_shortcode('worldcountries', [self::class, 'shortcode']);
    }

    /**
     * @param $atts
     * @param $content
     * @return false|string
     */
    public static function shortcode($atts, $content)
    {
        //register script and styles
        wp_enqueue_script( 'wc-datatables-js', plugins_url( 'assets/libs/DataTables/datatables.min.js', WC_PLUGIN_FILE ), ['jquery'], 1.0, true );

        wp_enqueue_style( 'wc-datatables-css', plugins_url( 'assets/libs/DataTables/datatables.min.css', WC_PLUGIN_FILE ) );

        return file_get_contents(WC_PLUGIN_DIR . 'templates/tables.php');
    }

    /**
     * @return bool|void
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function afterActivation()
    {
        $restCountriesApi = new RestCountriesApi();

        if($restCountriesApi->countriesExist()){
            return;
        }

        try {
            //create table
            $this->createDbTable();

            //save first data
            $countries = $restCountriesApi->getCountries();

            foreach ($countries as $key => $country) {
                $country->languages = json_encode($country->languages);
                $country->timezones = json_encode($country->timezones);

                $restCountriesApi->saveCountry($country);
            }
        } catch (\Exception $e) {
            return false;
        }



    }

    /**
     *
     */
    public function enqueue_scripts_styles()
    {
        //styles
        wp_enqueue_style( 'wc-style-css', plugins_url( 'assets/css/style.css', WC_PLUGIN_FILE ) );
        //scripts
        wp_enqueue_script( 'wc-scripts-js', plugins_url( 'assets/js/scripts.js', WC_PLUGIN_FILE ), ['jquery'], 1.0, true );
    }

    /**
     * @return array|object|null
     */
    public static function countries()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . self::DB_TABLE_NAME;
        $columnsSelect = implode(',', RestCountriesApi::REQUIRED_FIELDS);

        return $wpdb->get_results( "SELECT {$columnsSelect} FROM {$table_name}", ARRAY_A );
    }

    /**
     * @return bool
     */
    protected function createDbTable()
    {
        global $wpdb;

        $table_name = $wpdb->prefix . self::DB_TABLE_NAME;
        $charset_collate = $wpdb->get_charset_collate();

        try {
            $sql = "CREATE TABLE IF NOT EXISTS $table_name (
              id mediumint(9) NOT NULL AUTO_INCREMENT,
              name varchar(255) DEFAULT '' NOT NULL,
              capital varchar(100) DEFAULT '' NOT NULL,
              region varchar(75) DEFAULT '' NOT NULL,
              population int DEFAULT 0 NOT NULL,
              timezones text NOT NULL,
              languages text NOT NULL,
              PRIMARY KEY  (id)
            ) $charset_collate;";

            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            dbDelta($sql);

        } catch (\Exception $e) {
            print_r($e->getMessage());
            return false;
        }
    }
}