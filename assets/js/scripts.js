jQuery( function( $ ) {
    function initDataTable() {
         $('#dataTable').DataTable( {
            columns: [
                { data: 'name' },
                { data: 'capital' },
                { data: 'region' },
                { data: 'population' },
                { data: 'timezones' },
                { data: 'languages' }
            ],
            columnDefs: [
                { orderable: true, className: 'reorder', targets: 0 },
                { orderable: true, className: 'reorder', targets: 3 },
                { orderable: false, targets: '_all' }
            ],
            "processing": true,
            "serverSide": true,
            "ajax":{
                "url" : "/wp-json/world-countries/v1/countries/",
                "type": "GET",
                "dataSrc": ""
            }
         } );
    };

    function init() {
        if ($('#dataTable')) {
            initDataTable();
        }
    };

    $(function() {
        init();
    });
} );